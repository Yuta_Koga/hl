import sys
sys.path.append("C:\\koga\\python\\HL_koga_bicorn\\Hamiltonian_Learning\\subpy")
from HL_block_TF_cpu01 import*
model = Bayes_main()


#0:simulation, 1:experiment
model.mode = 0

#wireの選択
model.wire = 1

#0: not sweep, 1: sweep
model.sweep = 1

#project fileの指定
model.project_file = "Project_ODMR"
model.project = "C:\\koga\\python\\HL_koga_bicorn\\project\\"+model.project_file+".csv"

#ランダムに実験を選ぶ回数
"""
実際に繰り返す回数　をrandom_expに代入する
"""

random_exp = 0
model.main(random_exp)