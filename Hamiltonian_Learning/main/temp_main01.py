import sys
sys.path.append("C:\\data&prog\\koga\\repository\\HL\\Hamiltonian_Learning\\subpy")

#ハミルトニアンラーニング用自作クラス
from HL_block_TF_01 import*
model = Bayes_main()

#0:simulation, 1:experiment
model.mode = 0

#wireの選択
model.wire = 1
model.std = 0.05

#0: not sweep, 1: sweep
model.sweep = 0

#model.project_file = "Project_ODMR"
model.project_file = "Project_Bz_02_考察"


model.project = "C:\\data&prog\\koga\\repository\\HL\\project\\20190423\\"+model.project_file+".csv"

#ランダムに実験を選ぶ回数
"""
実際に繰り返す回数　をrandom_expに代入する
"""
random_exp = 0
model.main(random_exp)

#np.savetxt("C:\\data&prog\\koga\\保存用結果\\risk007_11.txt", model.risk)

