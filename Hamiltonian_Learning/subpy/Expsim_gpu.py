# -*- coding: utf-8 -*-
"""
Created on Sat Jan 26 09:42:18 2019

@author: a319
"""

def Expsim_gpu(H,t,S0,R0):
    """
    H: システムハミルトニアン + ドライブハミルトニアン
    t: マイクロ波のwidth
    S0: ms=0の射影演算子
    R0: 初期状態の密度行列
    """
    
    #変数の設定
    batchSize = int(H.shape[0]/H.shape[1])
    l = H.shape[1]
    
    import numpy as np
    import pycuda.autoinit
    import pycuda.gpuarray as gpuarray
    import skcuda.cusolver as solver
    import skcuda.linalg as culinalg
    import scipy.linalg as sp
    import time
    import skcuda.cublas as cublas
    
    culinalg.init()
    
    handle = solver.cusolverDnCreate()
    
    """
    A = np.empty((l*batchSize, l), dtype = np.complex64)
    B = np.empty((l*batchSize, l), dtype = A.dtype)
    
    #batch分エルミート行列
    for i in range(batchSize):
        #エルミート行列を生成
        x = np.random.randn(l, l)+1j*np.random.randn(l,l)
        x = x+x.conj().T
        x = x.astype(np.complex64)
        A[i*l:(i+1)*l, :] = x
        # Need to reverse dimensions because CUSOLVER expects column-major matrices:
        B[i*l:(i+1)*l, :] = x.T.copy()
    """
    
    #計測スタート
    t1 = time.time()
    
    #CPUからGPUに移す
    x_gpu = gpuarray.to_gpu(H)
    
    # Set up output buffers:
    w_gpu = gpuarray.empty((batchSize, l), dtype = np.float32)
    
    # Set up parameters
    #オブジェクトのリファレンスのようなもの？
    params = solver.cusolverDnCreateSyevjInfo()
    solver.cusolverDnXsyevjSetTolerance(params, 1e-7)
    solver.cusolverDnXsyevjSetMaxSweeps(params, 15)
    
    # Set up work buffers:
    lwork = solver.cusolverDnCheevjBatched_bufferSize(handle, 'CUSOLVER_EIG_MODE_VECTOR',
                                        'u', l, x_gpu.gpudata, l,
                                        w_gpu.gpudata, params, batchSize)
    
    workspace_gpu = gpuarray.zeros(lwork, dtype = np.complex64)
    info = gpuarray.zeros(batchSize, dtype = np.int32)
    
    # Compute:
    solver.cusolverDnCheevjBatched(handle, 'CUSOLVER_EIG_MODE_VECTOR',
                           'u', l, x_gpu.gpudata, l,
                            w_gpu.gpudata, workspace_gpu.gpudata,
                            lwork, info.gpudata, params, batchSize)
    
    # Print info
    tmp = info.get()
    if any(tmp):
        print("the following job did not converge: %r", np.nonzero(tmp)[0])
    else:
        print("all jobs converged")
    
    # Destroy handle
    solver.cusolverDnDestroySyevjInfo(params)
    solver.cusolverDnDestroy(handle)
    
    #GPUからCPUに情報を伝達
    Q = x_gpu.get()
    W = w_gpu.get()
    
    
    #debug
    Q_cpu = np.empty((batchSize, l, l), dtype = np.complex64)
    Q_inv_cpu = np.empty((batchSize, l, l), dtype = np.complex64)
    W_cpu = np.empty((batchSize, l, l), dtype = np.complex64)
    for i in range(batchSize):
        
        Q_cpu[i,:,:] = Q[i*l:(i+1)*l,:].T.copy()
        Q_inv_cpu[i,:,:] = np.conjugate(Q[i*l:(i+1)*l,:].copy())
        W_cpu[i,:,:] = np.diag(np.exp(-1j*W[i,:].copy()))
    
    """
    S0 = np.empty((batchSize, l, l), dtype = np.complex64)
    R0 = np.empty((batchSize, l, l), dtype = np.complex64)
    
    for i in range(batchSize):
        S0[i] = np.random.randn(l*l).reshape([l,l])
        R0[i] = np.random.randn(l*l).reshape([l,l])
    """
    
    
    
    #===============================積を計算========================================
    
    def bptrs(a):
        """
        Pointer array when input represents a batch of matrices.
        """
    
        return gpuarray.arange(a.ptr,a.ptr+a.shape[0]*a.strides[0],a.strides[0],
                    dtype=cublas.ctypes.c_void_p)
    
    handle = cublas.cublasCreate()
    
    C = np.empty((batchSize, l, l), dtype = np.complex64)
    U = np.empty((batchSize, l, l), dtype = np.complex64)
    E = np.empty((batchSize, l, l), dtype = np.complex64)
    F = np.empty((batchSize, l, l), dtype = np.complex64)
    G = np.empty((batchSize, l, l), dtype = np.complex64)
    H = np.empty((batchSize, l, l), dtype = np.complex64)
    
    
    #CPUからGPUに移す
    Q_gpu = gpuarray.to_gpu(Q_cpu)
    Q_inv_gpu = gpuarray.to_gpu(Q_inv_cpu)
    W_gpu = gpuarray.to_gpu(W_cpu)
    C_gpu = gpuarray.to_gpu(C) #バッファ
    U_gpu = gpuarray.to_gpu(U) #バッファ
    E_gpu = gpuarray.to_gpu(E) #バッファ
    F_gpu = gpuarray.to_gpu(F) #バッファ
    G_gpu = gpuarray.to_gpu(G) #時間発展後の状態
    S0_gpu = gpuarray.to_gpu(S0) #ms=0の射影演算子
    R0_gpu = gpuarray.to_gpu(R0) #初期状態
    
    #一回目の積
    t1 = time.time()
    #計算に必要なパラメータ
    transa = "T" #T
    transb = "T" #T
    m = l
    n = l
    k = l
    alpha = np.complex64(1.0)
    Q_arr = bptrs(Q_gpu)
    lda = l
    W_arr = bptrs(W_gpu)
    ldb = l
    C_arr = bptrs(C_gpu)
    ldc = l
    beta = np.complex64(0.0)
    batchCount = batchSize
    
    Q_inv_arr = bptrs(Q_inv_gpu)
    
    
    
    cublas.cublasCgemmBatched(handle, transa, transb, m, n, k,
                           alpha, W_arr.gpudata, lda, Q_inv_arr.gpudata, ldb, beta, C_arr.gpudata, ldc, batchCount)
    
    C_cpu = C_gpu.get()
    
    for i in range(batchSize):
        C_cpu[i,:,:] = C_cpu[i,:,:].T
    #print(C_cpu[10])
    
    
    #時間発展演算子計算
    C_gpu = gpuarray.to_gpu(C_cpu)
    C_arr = bptrs(C_gpu)
    U_arr = bptrs(U_gpu)
    
    transc = "T" #T
    transb = "T" #N
    
    cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                           alpha, Q_arr.gpudata, lda, C_arr.gpudata, ldb, beta, U_arr.gpudata, ldc, batchCount)
    
    U_cpu = U_gpu.get()
    
    #時間発展演算子の逆変換
    U_inv_cpu = np.conjugate(U_cpu)
    for i in range(batchSize):
        U_inv_cpu[i,:,:] = U_inv_cpu[i,:,:].T
        
    
    #左から時間発展演算子をかける
    E_arr = bptrs(E_gpu)
    R0_arr = bptrs(R0_gpu)
    
    transc = "T" #T
    transb = "T" #T
    
    cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                           alpha, U_arr.gpudata, lda, R0_arr.gpudata, ldb, beta, E_arr.gpudata, ldc, batchCount)
    
    
    E_cpu = E_gpu.get()
    for i in range(batchSize):
        E_cpu[i,:,:] = E_cpu[i,:,:].T
    
    #右から時間発展演算子の逆行列をかける
    transc = "T" #T
    transb = "T" #T
    
    E_gpu = gpuarray.to_gpu(E_cpu)
    U_inv_gpu = gpuarray.to_gpu(U_inv_cpu)
    E_arr = bptrs(E_gpu)
    F_arr = bptrs(F_gpu)
    U_inv_gpu_arr = bptrs(U_inv_gpu)
    
    
    cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                           alpha, E_arr.gpudata, lda, U_inv_gpu_arr.gpudata, ldb, beta, F_arr.gpudata, ldc, batchCount)
    
    
    F_cpu = F_gpu.get()
    for i in range(batchSize):
        F_cpu[i,:,:] = F_cpu[i,:,:].T
    
    #ms=0で射影する
    transc = "T" #T
    transb = "T" #T
    
    F_gpu = gpuarray.to_gpu(F_cpu)
    S0_gpu = gpuarray.to_gpu(S0)
    S0_arr = bptrs(S0_gpu)
    F_arr = bptrs(F_gpu)
    G_arr = bptrs(G_gpu)
    
    cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                           alpha, S0_arr.gpudata, lda, F_arr.gpudata, ldb, beta, G_arr.gpudata, ldc, batchCount)
    
    G_cpu = G_gpu.get()
    for i in range(batchSize):
        G_cpu[i,:,:] = G_cpu[i,:,:].T
        
    #handleの消去
    cublas.cublasDestroy(handle)
    
    #計測終了
    t2 = time.time()
    print(t2 - t1)
    
    #traceをとる
    p_table = np.empty([batchSize,1],dtype = np.complex64)
    for i in range(batchSize):
        p_table[i] = np.trace(G_cpu[i])
    t3 = time.time()
    print(t3 - t2)
    
    return p_table
    
