# -*- coding: utf-8 -*-
"""
Created on Sat Jan 26 09:42:18 2019

@author: a319
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 11:38:11 2019

@author: a319
"""

"""
Demo of how to call low-level CUSOLVER wrappers to perform eigen decomposition
for a batch of small Hermitian matrices.
"""

import numpy as np
import pycuda.autoinit
import pycuda.gpuarray as gpuarray
import skcuda.cusolver as solver
import skcuda.linalg as culinalg
import scipy.linalg as sp
import time

culinalg.init()



for k in range(3):
    
    handle = solver.cusolverDnCreate()
    batchSize = 100000
    l = 3
    
    A = np.empty((l*batchSize, l), dtype = np.complex64)
    B = np.empty((l*batchSize, l), dtype = A.dtype)
    
    #batch分エルミート行列
    for i in range(batchSize):
        #エルミート行列を生成
        x = np.random.randn(l, l)+1j*np.random.randn(l,l)
        x = x+x.conj().T
        x = x.astype(np.complex64)
        A[i*l:(i+1)*l, :] = x
        # Need to reverse dimensions because CUSOLVER expects column-major matrices:
        B[i*l:(i+1)*l, :] = x.T.copy()
    
    #計測スタート
    t1 = time.time()
    
    #CPUからGPUに移す
    x_gpu = gpuarray.to_gpu(B)
    
    # Set up output buffers:
    w_gpu = gpuarray.empty((batchSize, l), dtype = np.float32)
    
    # Set up parameters
    #オブジェクトのリファレンスのようなもの？
    params = solver.cusolverDnCreateSyevjInfo()
    solver.cusolverDnXsyevjSetTolerance(params, 1e-7)
    solver.cusolverDnXsyevjSetMaxSweeps(params, 15)
    
    # Set up work buffers:
    lwork = solver.cusolverDnCheevjBatched_bufferSize(handle, 'CUSOLVER_EIG_MODE_VECTOR',
                                        'u', l, x_gpu.gpudata, l,
                                        w_gpu.gpudata, params, batchSize)
    
    workspace_gpu = gpuarray.zeros(lwork, dtype = A.dtype)
    info = gpuarray.zeros(batchSize, dtype = np.int32)
    
    # Compute:
    solver.cusolverDnCheevjBatched(handle, 'CUSOLVER_EIG_MODE_VECTOR',
                           'u', l, x_gpu.gpudata, l,
                            w_gpu.gpudata, workspace_gpu.gpudata,
                            lwork, info.gpudata, params, batchSize)
    
    # Print info
    tmp = info.get()
    if any(tmp):
        print("the following job did not converge: %r", np.nonzero(tmp)[0])
    else:
        print("all jobs converged")
    
    # Destroy handle
    solver.cusolverDnDestroySyevjInfo(params)
    solver.cusolverDnDestroy(handle)
    
    #GPUからCPUに情報を伝達
    Q = x_gpu.get()
    W = w_gpu.get()
    
    
    #debug
    Q_cpu = np.empty((batchSize, l, l), dtype = A.dtype)
    Q_inv_cpu = np.empty((batchSize, l, l), dtype = A.dtype)
    W_cpu = np.empty((batchSize, l, l), dtype = A.dtype)
    for i in range(batchSize):
        
        Q_cpu[i,:,:] = Q[i*l:(i+1)*l,:].T.copy()
        Q_inv_cpu[i,:,:] = np.conjugate(Q[i*l:(i+1)*l,:].copy())
        W_cpu[i,:,:] = np.diag(np.exp(-1j*W[i,:].copy()))
    
    
    S0 = np.empty((batchSize, l, l), dtype = A.dtype)
    R0 = np.empty((batchSize, l, l), dtype = A.dtype)
    
    for i in range(batchSize):
        S0[i] = np.random.randn(l*l).reshape([l,l])
        R0[i] = np.random.randn(l*l).reshape([l,l])
    
    
    for i in range(batchSize):
        q = Q[i*l:(i+1)*l,:].T.copy()
        w = W[i, :].copy()
        ew_ = np.exp(-1j*w)
        ew_ = np.diag(ew_)
        q_ = np.conjugate(q.T)
        D_ = np.dot(ew_, q_)
        D = np.dot(q, D_).T #時間発展演算子
        D_inv = np.conjugate(D.T)
        if i==0:
            """
            print("numpyD")
            print(D)
            
            print("numpyE")
            print(np.dot(D, R0[0]))
            
            print("numpyF")
            print(np.dot( np.dot(D, R0[0]), D_inv))
            
            print("numpuG")
            print(np.dot( S0[0], np.dot( np.dot(D, R0[0]), D_inv)))
            """
    
    
    
    import numpy as np
    import pycuda.autoinit
    import pycuda.gpuarray as gpuarray
    import skcuda.cublas as cublas
    import skcuda.linalg as culinalg
    import time
    
    culinalg.init()
    
    def bptrs(a):
        """
        Pointer array when input represents a batch of matrices.
        """
    
        return gpuarray.arange(a.ptr,a.ptr+a.shape[0]*a.strides[0],a.strides[0],
                    dtype=cublas.ctypes.c_void_p)
    
    handle = cublas.cublasCreate()
    
    C = np.empty((batchSize, l, l), dtype = A.dtype)
    D = np.empty((batchSize, l, l), dtype = A.dtype)
    E = np.empty((batchSize, l, l), dtype = A.dtype)
    F = np.empty((batchSize, l, l), dtype = A.dtype)
    G = np.empty((batchSize, l, l), dtype = A.dtype)
    H = np.empty((batchSize, l, l), dtype = A.dtype)
    
    
    #CPUからGPUに移す
    Q_gpu = gpuarray.to_gpu(Q_cpu)
    Q_inv_gpu = gpuarray.to_gpu(Q_inv_cpu)
    W_gpu = gpuarray.to_gpu(W_cpu)
    C_gpu = gpuarray.to_gpu(C)
    D_gpu = gpuarray.to_gpu(D)
    E_gpu = gpuarray.to_gpu(E)
    F_gpu = gpuarray.to_gpu(F)
    G_gpu = gpuarray.to_gpu(G)
    H_gpu = gpuarray.to_gpu(H)
    S0_gpu = gpuarray.to_gpu(S0)
    R0_gpu = gpuarray.to_gpu(R0)
    
    
    #計算に必要なパラメータ
    transa = "T" #T
    transb = "T" #T
    m = l
    n = l
    k = l
    alpha = np.complex64(1.0)
    Q_arr = bptrs(Q_gpu)
    lda = l
    W_arr = bptrs(W_gpu)
    ldb = l
    C_arr = bptrs(C_gpu)
    ldc = l
    beta = np.complex64(0.0)
    batchCount = batchSize
    
    Q_inv_arr = bptrs(Q_inv_gpu)
    
    t1 = time.time()
    #一回目の積
    cublas.cublasCgemmBatched(handle, transa, transb, m, n, k,
                           alpha, W_arr.gpudata, lda, Q_inv_arr.gpudata, ldb, beta, C_arr.gpudata, ldc, batchCount)
    
    C_cpu = C_gpu.get()
    """
    #debug
    A_cpu = W_gpu.get()
    B_cpu = Q_inv_gpu.get()
    print("C_cpu[0]")
    print(np.dot(A_cpu[10], B_cpu[10]))
    print("\n")
    """
    
    
    for i in range(batchSize):
        C_cpu[i,:,:] = C_cpu[i,:,:].T
    #print(C_cpu[10])
    
    
    #二回目の積
    C_gpu = gpuarray.to_gpu(C_cpu)
    C_arr = bptrs(C_gpu)
    D_arr = bptrs(D_gpu)
    
    transc = "T" #T
    transb = "T" #N
    
    cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                           alpha, Q_arr.gpudata, lda, C_arr.gpudata, ldb, beta, D_arr.gpudata, ldc, batchCount)
    
    D_cpu = D_gpu.get()
    """
    print("D_cpu[0]")
    print(D_cpu[0])
    print("\n")
    """
    
    D_inv_cpu = np.conjugate(D_cpu)
    for i in range(batchSize):
        D_inv_cpu[i,:,:] = D_inv_cpu[i,:,:].T
        
    """
    #debug
    print(D_cpu[10])
    print(np.dot( np.dot(A_cpu[10], B_cpu[10]) ,B_cpu[10]))
    print("\n")
    """
    
    
    """
    #debug
    #numpy arrayと比較
    lam, vec = np.linalg.eig(B[0:l,:])
    w = np.exp(-1j*lam)
    w = np.diag(w)
    X1 = np.dot(vec, np.dot(w, np.linalg.inv(vec)))
    #print("X1")
    #print(X1)
    #print("\n")
    
    eB = sp.expm(-1j*B[0:l,:])
    """
    
    #ms=0に射影
    #3回目の積
    E_arr = bptrs(E_gpu)
    R0_arr = bptrs(R0_gpu)
    
    transc = "T" #T
    transb = "T" #T
    
    cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                           alpha, D_arr.gpudata, lda, R0_arr.gpudata, ldb, beta, E_arr.gpudata, ldc, batchCount)
    
    
    E_cpu = E_gpu.get()
    for i in range(batchSize):
        E_cpu[i,:,:] = E_cpu[i,:,:].T
    
    """
    #debug
    print(D_cpu[10])
    print(np.dot( np.dot(A_cpu[10], B_cpu[10]) ,B_cpu[10]))
    print("\n")
    """
    
    
    transc = "T" #T
    transb = "T" #T
    
    E_gpu = gpuarray.to_gpu(E_cpu)
    D_inv_gpu = gpuarray.to_gpu(D_inv_cpu)
    E_arr = bptrs(E_gpu)
    F_arr = bptrs(F_gpu)
    D_inv_gpu_arr = bptrs(D_inv_gpu)
    
    #時間発展
    #4回目の積
    cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                           alpha, E_arr.gpudata, lda, D_inv_gpu_arr.gpudata, ldb, beta, F_arr.gpudata, ldc, batchCount)
    
    
    F_cpu = F_gpu.get()
    for i in range(batchSize):
        F_cpu[i,:,:] = F_cpu[i,:,:].T
    """
    #debug
    print("F_cpu[0]")
    print(F_cpu[0])
    """
    
    
    transc = "T" #T
    transb = "T" #T
    
    
    F_gpu = gpuarray.to_gpu(F_cpu)
    S0_gpu = gpuarray.to_gpu(S0)
    S0_arr = bptrs(S0_gpu)
    F_arr = bptrs(F_gpu)
    G_arr = bptrs(G_gpu)
    
    #時間発展
    #5回目の積
    cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                           alpha, S0_arr.gpudata, lda, F_arr.gpudata, ldb, beta, G_arr.gpudata, ldc, batchCount)
    
    
    G_cpu = G_gpu.get()
    for i in range(batchSize):
        G_cpu[i,:,:] = G_cpu[i,:,:].T
    """
    #debug
    print("G_cpu[0]")
    print(G_cpu[0])
    """
    
    """
    transc = "T" #T
    transb = "T" #T
    
    
    G_gpu = gpuarray.to_gpu(G_cpu)
    H_arr = bptrs(H_gpu)
    G_arr = bptrs(G_gpu)
    
    #時間発展
    #6回目の積
    cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                           alpha, G_arr.gpudata, lda, S0_arr.gpudata, ldb, beta, H_arr.gpudata, ldc, batchCount)
    
    
    H_cpu = H_gpu.get()
    for i in range(batchSize):
        H_cpu[i,:,:] = H_cpu[i,:,:].T
    print("H_cpu[0]")
    print(H_cpu[0])
    """
    
    
    #
    t2 = time.time()
    print(t2 - t1)
    
    
    #debug
    #numpy arrayと比較
    lam, vec = np.linalg.eig(B[0:l,:])
    w = np.exp(-1j*lam)
    w = np.diag(w)
    U = np.dot(vec, np.dot(w, np.linalg.inv(vec)))
    U_inv = np.linalg.inv(U)
    R = np.dot( S0[0], np.dot( np.dot(U, R0[0]),U_inv))
    RR = np.dot( np.dot( S0[0], np.dot( np.dot(U, R0[0]),U_inv)),S0[0])
    
    
    #==================================trace==================================
    p_table = np.empty([batchSize,1],dtype = np.complex64)
    for i in range(batchSize):
        p_table[i] = np.trace(G_cpu[i])
    t3 = time.time()
    print(t3 - t2)
    


    #handleの消去
    cublas.cublasDestroy(handle)
