# -*- coding: utf-8 -*-
"""
Created on Wed Jan 30 12:11:19 2019

@author: a319
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 23:38:45 2019

@author: a319
"""

import numpy as np
from Q_module_new02 import*
import scipy.linalg as sp

"""
一時的な実装です！xやは前回実行時のBayes_main()クラスから無理やり持ってきてます
"""
x=model.x
n=len(x)
m=model.n_exp(model.exp_select)
l=x.shape[1]
C=model.C[0]

"""
現状の実装
"""

def H_0(x):
    He=x[5]*tensor(Sz*Sz,III)
    Hn=x[7]*tensor(III,Sz*Sz)
    Hint_en=x[6]*tensor(Sz,Sz)
    HB=x[8]*tensor(Sz,III) #x[8]:Bzs
    H0=He+Hn+Hint_en+HB
    return H0

def R_V_func(a,b,V):
    if b > V:
        Ome=a*(b*V - (V**2)/2.0)
    else:
        Ome=1/2.0 * a *b**2
    return Ome

def Vdrive_all(x,V1,V2,phi):
    if model.wire==1:
        model.Ome1=model.R_V_func(x[0],x[1],V1)
        model.Vd=model.Ome1/2.0*Sx
    if len(model.Ac_list) != 0:
        model.Vd=tensor(model.Vd,III,model.C_mat)
    else:
        model.Vd=tensor(model.Vd,III)
    return model.Vd

def H_rot(omega): #回転座標系に乗せるための行列を生成する関数
    mw=III
    rf=III
    if model.pulse==1:
        rf=Sz*Sz
    else:
        mw=Sz*Sz
    if len(model.Ac_list) != 0:
        model.Hf=omega*tensor(mw,rf,model.C_mat)
    else:
        model.Hf=omega*tensor(mw,rf)
        
    return model.Hf


print("現状実装計測開始")
t0 = time.time()
H0_list_temp = np.empty([n * m * l, l],dtype = np.complex64)
Vd_list_temp = np.empty([n * m * l, l],dtype = np.complex64)
Hf_list_temp = np.empty([n * m * l, l],dtype = np.complex64)

for i in range(n):
    for j in range(m):
        H0_list_temp[(i*m+j)*l:(i*m+j+1)*l,:] = H_0(x[i]).full()
        Vd_list_temp[(i*m+j)*l:(i*m+j+1)*l,:] = Vdrive_all(x[i],C[j][0],C[j][1],C[j][2]).full()
        Hf_list_temp[(i*m+j)*l:(i*m+j+1)*l,:] = H_rot(C[j][3]).full()

HI_list_temp = H0_list_temp + Vd_list_temp - Hf_list_temp

t1 = time.time()
print("現状実装計測終了")
print("Creating Hamiltonian time:{}s".format(t1 - t0))







print("提案実装計測開始")
t0 = time.time()

x_m=np.einsum("i, jk -> jik", np.ones(m), x)
x_m=x_m.reshape(n*m,x.shape[1])
x2=x_m.T

"""
einsumを一つにして実装
炭素を加えて実装
"""

#炭素数に応じてハミルトニアンの骨格を拡張
if len(model.Ac_list) != 0:
    #炭素数分恒等行列を作成
    C_list = []
    for i in range(len(model.Ac_list)):
        C_list.append(2)
        model.C_mat=Qobj(qeye(2**len(model.Ac_list)),dims=[C_list,C_list])
    
    #炭素のハイパーファインのシード作成
    Hint_ec_seed = []
    for i in range(len(model.Ac_list)):
        C_z=tensor(Sz,III) #電子、窒素
        for j in range(i):
            C_z=tensor(C_z,II)
        C_z=tensor(C_z,sigz)
        for k in range(len(model.Ac_list)-i-1):
            C_z=tensor(C_z,II)
        Hint_ec_seed.append(C_z.full())
        
    #各シードに炭素分の恒等演算子を追加
    He_seed =       (tensor(Sz*Sz,III,model.C_mat)).full()
    Hn_seed =       (tensor(III,Sz*Sz,model.C_mat)).full()
    Hint_en_seed =  (tensor(Sz,Sz,model.C_mat)).full()
    HB_seed =       (tensor(Sz,III,model.C_mat)).full()
    
    H_seed = [He_seed, Hint_en_seed, Hn_seed, HB_seed]
    for i in range(len(model.Ac_list)):
        H_seed.append(Hint_ec_seed[i])
        
    H_seed = np.array(H_seed)
    
else:
    He_seed =       (tensor(Sz*Sz,III)).full()
    Hn_seed =       (tensor(III,Sz*Sz)).full()
    Hint_en_seed =  (tensor(Sz,Sz)).full()
    HB_seed =       (tensor(Sz,III)).full()
    
    H_seed = [He_seed, Hint_en_seed, Hn_seed, HB_seed]
    H_seed = np.array(H_seed)

#ハミルトニアンの大きさ
seed_size=He_seed.shape[0]

x3 = x2[5:,:].T
H_tensor = np.einsum("ij, jkl -> ijkl", x3, H_seed)
H_tensor = np.sum(H_tensor, axis = 1)
H = np.reshape(H_tensor,(seed_size*n*m,seed_size))

"""
ドライブハミルトニアン生成
"""

x = model.x
n = len(x)
m = model.n_exp(model.exp_select)
C = model.C[0]

x1 = x[:,:5]

def R_V_func(a,b,V):
    if b > V:
        Ome=a*(b*V - (V**2)/2.0)
    else:
        Ome=1/2.0 * a *b**2
    return Ome

#ラビ周波数
Omega = np.array([R_V_func(x1[i,0],x1[i,1],C[j,0])/2.0 for i in range(n) for j in range(m)])

if len(model.Ac_list) != 0:
    Hd_seed = (tensor(Sx,III,model.C_mat)).full()
else:
    Hd_seed = (tensor(Sx,III)).full()
    
Hd_tensor = np.einsum("i, jk -> ijk", Omega, Hd_seed)

Hd = np.reshape(Hd_tensor,(seed_size*n*m,seed_size))

"""
回転座標系にのせるためのハミルト二アン生成
"""

#MW周波数
MWf = np.array([C[j,4] for i in range(n) for j in range(m)])

if len(model.Ac_list) != 0:
    Hf_seed = (tensor(Sz*Sz,III,model.C_mat)).full()
else:
    Hf_seed = (tensor(Sz*Sz,III)).full()
    
Hf_tensor = np.einsum("i, jk -> ijk", MWf, Hf_seed)

Hf = np.reshape(Hf_tensor,(seed_size*n*m,seed_size))

"""
最終的なハミルトニアン生成
"""
HI = H + Hd - Hf

"""
初期状態を複数実装
"""

#初期化
model.rho_init()

rho0_seed = (model.rho).full()

expand = np.ones(n*m)

rho0 = np.einsum("i, jk -> ijk", expand, rho0_seed)

"""
射影演算子を複数実装
"""

M0 = rho0 * seed_size/3

t1 = time.time()
print("Creating Hamiltonian time:{}s".format(t1 - t0))


"""
時間発展してtrace
"""
ptable = np.empty([1,n*m])
for i in range(n*m):
    U = sp.expm(-1j*HI[i*9:(i+1)*9, :]*0.2*2*np.pi)
    U_inv = np.conjugate(U.T)
    ptable[:,i] = np.trace( np.dot( M0[i], np.dot( np.dot(U, rho0[i]), U_inv ) ) )

"""
現状実装で時間発展してtrace
""" 
ptable_temp = np.empty([1,n*m])
for i in range(n*m):
    U = sp.expm(-1j*HI[i*9:(i+1)*9, :]*0.2*2*np.pi)
    U_inv = np.conjugate(U.T)
    ptable_temp[:,i] = np.trace( np.dot( M0[i], np.dot( np.dot(U, rho0[i]), U_inv ) ) )

