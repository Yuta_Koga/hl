# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 09:20:56 2019

@author: a319
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 11:38:11 2019

@author: a319
"""

"""
Demo of how to call low-level CUSOLVER wrappers to perform eigen decomposition
for a batch of small Hermitian matrices.
"""

import numpy as np
import pycuda.autoinit
import pycuda.gpuarray as gpuarray
import pycuda.cumath as cumath
import skcuda.cusolver as solver
import skcuda.linalg as culinalg
import time
import scipy.linalg as sp

culinalg.init()

handle = solver.cusolverDnCreate()
batchSize = 100000
n = 18

A = np.empty((n*batchSize, n), dtype = np.complex64)
B = np.empty((n*batchSize, n), dtype = A.dtype)
C = np.ones((n, n),dtype = A.dtype)

#batch分エルミート行列
for i in range(batchSize):
    #エルミート行列を生成
    x = np.random.randn(n, n)+1j*np.random.randn(n,n)
    x = x+x.conj().T
    x = x.astype(np.complex64)
    A[i*n:(i+1)*n, :] = x
    # Need to reverse dimensions because CUSOLVER expects column-major matrices:
    B[i*n:(i+1)*n, :] = x.T.copy()
    



#CPUからGPUに移す
x_gpu = gpuarray.to_gpu(B)

# Set up output buffers:
w_gpu = gpuarray.empty((batchSize, n), dtype = np.float32)

start = time.time()

# Set up parameters
#オブジェクトのリファレンスのようなもの？
params = solver.cusolverDnCreateSyevjInfo()
solver.cusolverDnXsyevjSetTolerance(params, 1e-7)
solver.cusolverDnXsyevjSetMaxSweeps(params, 15)

# Set up work buffers:
lwork = solver.cusolverDnCheevjBatched_bufferSize(handle, 'CUSOLVER_EIG_MODE_VECTOR',
                                    'u', n, x_gpu.gpudata, n,
                                    w_gpu.gpudata, params, batchSize)

workspace_gpu = gpuarray.zeros(lwork, dtype = A.dtype)
info = gpuarray.zeros(batchSize, dtype = np.int32)

# Compute:
solver.cusolverDnCheevjBatched(handle, 'CUSOLVER_EIG_MODE_VECTOR',
                       'u', n, x_gpu.gpudata, n,
                        w_gpu.gpudata, workspace_gpu.gpudata,
                        lwork, info.gpudata, params, batchSize)

# Print info
tmp = info.get()
if any(tmp):
    print("the following job did not converge: %r", np.nonzero(tmp)[0])
else:
    print("all jobs converged")

# Destroy handle
solver.cusolverDnDestroySyevjInfo(params)
solver.cusolverDnDestroy(handle)

gpu_end = time.time()
print("GPU_diag_time")
print(gpu_end - start)



gpu_to_cpu = time.time()
print("GPU_to_CPU_time")
print(gpu_to_cpu - gpu_end)

t0 = time.time()
for i in range(batchSize):
    l, P = np.linalg.eig(A[i*n:(i+1)*n, :])
t1 = time.time()
print("CPU_time:{}s".format(t1 - t0))

#GPUからCPUに情報を伝達
Q = x_gpu.get()
W = w_gpu.get()


"""
for i in range(batchSize):
    #各固有値の指数の計算
    W1 = w_gpu[i].astype(np.complex64)
    
    #指数を計算
    W2 = cumath.exp(-2j*W1)
    
    #対角行列に変換
    #eW2 = linalg.diag(W2)
    
    #元の行列の指数を計算
    q_gpu = x_gpu[i*n:(i+1)*n,:].transpose()
    #wq_gpu = linalg.dot(eW2,linalg.inv(q_gpu).T)
    

#print('maximum error in A * Q - Q * Lambda is:')
for i in range(batchSize):
    t1=time.time()
    q = Q[i*n:(i+1)*n,:].T.copy()
    w = W[i, :].copy()
    t2=time.time()
    #print('{}th matrix %r'.format(i) % np.abs(np.dot(x, q) - np.dot(q, np.diag(w))).max())
    #numpyに戻して比較
    ew_ = np.exp(-2j*w)
    ew_ = np.diag(ew_)
    t3 = time.time()
    #q_inv = np.linalg.inv(q)
    t4 = time.time()
    q_ = np.conjugate(q.T)
    D_ = np.dot(ew_, q_)
    t5 = time.time()
    D = np.dot(q, D_).T
    if i==0:
        print(D)
    t6 = time.time()

numpy_end = time.time()
print("exponential_end_gpu")
print(numpy_end - gpu_to_cpu)

#numpy arrayと比較
lam, vec = np.linalg.eig(B[0:n,:])
w = np.exp(-2j*lam)
w = np.diag(w)
X1 = np.dot(vec, np.dot(w, np.linalg.inv(vec)))
    
numpy_diag = time.time()
print("exponential_end_cpu")
print(numpy_diag - numpy_end)
eB = sp.expm(-2j*B[0:n,:])
"""

