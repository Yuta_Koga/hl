# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 23:38:45 2019

@author: a319
"""

import numpy as np
from Q_module_new02 import*

"""
一時的な実装です！xやは前回実行時のBayes_main()クラスから無理やり持ってきてます
"""
x=model.x
n=len(x)
l = 3*3
m=model.n_exp(model.exp_select)

"""
現状実装

欠点
：単純にFor文が遅い
：for文内で利用している関数H_0がqutipのオブジェクトのため、numpyの高速なメソッドやGPU並列計算に対応できない
"""


def H_0(x):
    He=x[5]*tensor(Sz*Sz,III)
    Hn=x[7]*tensor(III,Sz*Sz)
    Hint_en=x[6]*tensor(Sz,Sz)
    HB=x[8]*tensor(Sz,III) #x[8]:Bzs
    H0=He+Hn+Hint_en+HB
    return H0


print("現状実装計測開始")
t0 = time.time()
H0_list_temp = np.empty([n * m * l, l],dtype = np.complex64)

for i in range(n):
    for j in range(m):
        H0_list_temp[(i*m+j)*l:(i*m+j+1)*l,:] = H_0(x[i]).full()


t1 = time.time()
print("現状実装計測終了")
print("Creating Hamiltonian time:{}s".format(t1 - t0))



"""
提案実装(numpy準拠)
numpyのメソッドを利用するだけで、一応現実的な計算時間に収まるようになりました。(10分⇒5秒)
全部numpyで書かれるから、GPUでの計算にも移行しやすい形だと思います
"""


print("提案実装計測開始")
t0 = time.time()

"""
1．まずは各ハミルトニアンに共通するseedを作成(ndarrayで)
"""
He_seed =       (tensor(Sz*Sz,III)).full()
Hn_seed =       (tensor(III,Sz*Sz)).full()
Hint_en_seed =  (tensor(Sz,Sz)).full()
HB_seed =       (tensor(Sz,III)).full() #x[8]:Bzs

seed_size=He_seed.shape[0]

"""
2.各seedにベクトルとして掛けられるようにxを転置　ついでにアインシュタイン積でm分だけxを拡張
"""
x_m=np.einsum("i, jk -> jik", np.ones(m), x)
x_m=x_m.reshape(n*m,x.shape[1])
x2=x_m.T


"""
3.アインシュタイン積を利用して各seedとxから三階テンソルの形で演算結果を出力
足し合わせてH0_tensorを作成
reshapeして完成

アインシュタイン積(np.einsm)は簡単に説明するとベクトルや行列、テンソルの積や和の方法を任意に定義できる関数です

アインシュタイン積をちゃんと利用すれば、足し合わしてreshapeする部分までを一つの式にまとめられるが、ただでさえ何やってる分かり難いのがさらに難解になるため割愛
ただし、AC_listが増えてもコードをいじらなくて済むようにするためにはそれの操作が必要か
H_tensor=[He_seed,HN_seed,....]
x_2dmatrix

"""

He_tensor=np.einsum("i, jk -> ijk", x2[5], He_seed)
Hn_tensor=np.einsum("i, jk -> ijk", x2[7], Hn_seed)
Hint_en_tensor=np.einsum("i, jk -> ijk", x2[6], Hint_en_seed)
HB_tensor=np.einsum("i, jk -> ijk", x2[8], HB_seed)

H0_tensor=He_tensor+Hn_tensor+Hint_en_tensor+HB_tensor

H0=np.reshape(H0_tensor,(seed_size*n*m,seed_size))

t1 = time.time()
print("提案実装計測終了")
print("Creating Hamiltonian time:{}s".format(t1 - t0))