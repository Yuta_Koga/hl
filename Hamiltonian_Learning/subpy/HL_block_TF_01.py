# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 07:39:48 2018

@author: yuta
"""
#GPUで行列の指数を計算するためのクラス
from Bayes_function_TF_gpu import*

#csvファイル読み込みのためのライブラリ
import sys
import copy as cp
from itertools import chain

class Bayes_main(Bayes_GPU):
    def __init__(self):
        #Bayes_Functionクラスの変数、メソッドを引き継ぐ
        Bayes_GPU.__init__(self)
        self.project = "C:\\data&prog\\koga\\repository\\HL\\project\\"+"Project_Bz_01.csv"
        
    def read_project(self):
        
        #data = pd.read_csv("C:\koga" + "\Project_Test01.csv")
        
        #プロジェクトファイルを読み込む
        f=open(self.project,"r",encoding="utf-8")
        temp=f.read().split(",")
        n=len(temp)
        data=[temp[i].split("\n") for i in range(n)]
        data=list(chain.from_iterable(data))
        data[0]='ex'
        f.close()
        
        #変数list
        variable=["ex","d","a","resample_threshold","approx_ratio","a1","b1","a2","b2",\
                  "w_theta","D0","AN","QN","Bz","Ac_list","n_Ac_list","Range_Ac_list",\
                  "n","ParamH","RangeH","V1","V2","phi","t","MWf","tw","g","ParamC",\
                  "RangeC","Limit","exp_flag","exp_select","bayes_threshold"]
        
        #推定パラメータリスト
        param_list=["a1","b1","a2","b2","w_theta","D0","AN","QN","Bz"]
        
        #実験設計パラメータリスト
        exp_list=["V1","V2","phi","MWwidth","MWfreq","tw"]
        
        #余分な要素を削除
        while ('' in data):
            data.remove('')
        
        print(data)
        
        #読み取った値を変数に格納
        Param_dict={}
        for i in variable:
            idx=data.index(i)
            if i=="Ac_list" or i=="n_Ac_list" or i=="Range_Ac_list":
                n=data.index("n") - data.index("Range_Ac_list")
                l_Ac=data[idx+1:idx+n]
                if i=="n_Ac_list":
                    Param_dict[i]=[int(l_Ac[i]) for i in range(n-1)]
                else:
                    Param_dict[i]=[float(l_Ac[i]) for i in range(n-1)]
                
            elif i=="n" or i=="ParamH" or i=="RangeH":
                dic={}
                for j,p in enumerate(param_list):
                    if i=="RangeH":
                        dic[p]=float(data[idx+1+j])
                    else:
                        dic[p]=int(data[idx+1+j])
                Param_dict[i]=dic
                
            elif i=="g" or i=="ParamC" or i=="RangeC":
                dic={}
                for j,p in enumerate(exp_list):
                    dic[p]=float(data[idx+1+j])
                Param_dict[i]=dic
                
            elif i=="Limit":
                dic={}
                for j,p in enumerate(exp_list):
                    for k in range(2):
                        if k==0:
                            dic[p+"min"]=float(data[idx+1+j*2+k])
                        else:
                            dic[p+"max"]=float(data[idx+1+j*2+k])
                Param_dict[i]=dic
                
            else:
                Param_dict[i]=data[idx+1]
        
        #読み取った文字列を数値に変換して代入
        self.ex=int(Param_dict["ex"])
        self.d=int(Param_dict["d"])
        self.a=float(Param_dict["a"])
        self.resample_threshold=float(Param_dict["resample_threshold"])
        self.approx_ratio=float(Param_dict["approx_ratio"])
        self.a1=float(Param_dict["a1"])
        self.b1=float(Param_dict["b1"])
        self.a2=float(Param_dict["a2"])
        self.b2=float(Param_dict["b2"])
        self.w_theta=float(Param_dict["w_theta"])
        self.D0=float(Param_dict["D0"])
        self.AN=float(Param_dict["AN"])
        self.QN=float(Param_dict["QN"])
        self.Bz=float(Param_dict["Bz"])
        self.Ac_list=Param_dict["Ac_list"]
        self.n_Ac_list=Param_dict["n_Ac_list"]
        self.Range_Ac_list=Param_dict["Range_Ac_list"]
        self.n=Param_dict["n"]
        self.ParamH=Param_dict["ParamH"]
        self.RangeH=Param_dict["RangeH"]
        self.V1=float(Param_dict["V1"])
        self.V2=float(Param_dict["V2"])
        self.phi=float(Param_dict["phi"])
        self.t=float(Param_dict["t"])
        self.MWf=float(Param_dict["MWf"])
        self.tw=float(Param_dict["tw"])
        self.g=Param_dict["g"]
        self.ParamC=Param_dict["ParamC"]
        self.RangeC=Param_dict["RangeC"]
        self.Limit=Param_dict["Limit"]
        self.exp_flag=Param_dict["exp_flag"]
        self.exp_select=Param_dict["exp_select"]
        self.bayes_threshold=float(Param_dict["bayes_threshold"])
    
        
    def estimation(self):
        return self.Mean(self.w,self.x)
    
    def initialize(self):
        #ハミルトニアンパラメータの変更を反映
        self.params()
        
        #真のハミルトニアンに炭素追加
        self.init_x0() 
        print("x0")
        print(self.x0)
        
        #パーティクルに炭素追加
        self.init_x()
        print("パーティクルの中心値")
        print(self.x)
        
        #実験設計の初期化
        self.init_C()
        
        #重みの初期化
        self.init_w()
        
        #効用の初期化
        self.init_U()
        
    def preparation(self):
        #パーティクルの作成
        self.x=self.Particlemaker(self.x,self.n,self.ParamH,self.RangeH)
        
        #ベイズリスクを計算する際の重み行列を初期化, パーティクル生成後に呼び出す
        self.weighting_matrix()
        
        #実験候補の作成
        self.C=self.Expmaker() 
        
    def Estimation_result(self):
        plt.grid(b="off")
        #推定パラメータの推移を表示
        for j,p in enumerate(self.ParamH):
            if self.ParamH[p]==1:
                self.Region_edge(0.95,p)
                self.result_dict[p].append(self.Mean(self.w,self.x)[j])
                plt.fill_between(self.i_list,self.Region_edge_output(p,1),self.Region_edge_output(p,0),color='lightgreen',alpha=0.3)
                #plt.hlines(self.x0_dict[p],0,self.i_list[self.i],"r",label="True_"+p) #真値の表示
                plt.xlabel("iteration number",fontsize=20)
                plt.ylabel(p,fontsize=20)
                plt.plot(self.i_list,self.result_dict[p],label="Estimated_"+p)
                plt.legend(loc="upper right")
                plt.title("Estimated_"+p,fontsize=24)
                plt.grid(b="off")

                plt.show()
    
        if self.ParamH["a2"] == 1:
            #重みの二次元表示
            w_=self.w.reshape(self.w.shape[0],)
            plt.xlim(self.x_dict["a2"] - self.RangeH["a2"]/2-1,self.x_dict["a2"] + self.RangeH["a2"]/2+1)
            plt.xlabel("a2",fontsize=20)
            plt.ylim(self.x_dict["b2"] - self.RangeH["b2"]/2-0.05,self.x_dict["b2"] + self.RangeH["b2"]/2+0.05)
            plt.ylabel("b2",fontsize=20)
            plt.title("Weight Map",fontsize=24)
            cm = self.generate_cmap(['mediumblue', 'limegreen', 'orangered'])
            plt.scatter(self.x.T[2], self.x.T[3], s=15, c=w_, cmap=cm)
            
            # カラーバーを表示
            ax=plt.colorbar()
            ax.set_label("Probability",fontsize=20)
            plt.show()
            
        elif self.ParamH["a1"] == 1:
            #重みの二次元表示
            w_=self.w.reshape(self.w.shape[0],)
            plt.xlim(self.x_dict["a1"] - self.RangeH["a1"]/2-1,self.x_dict["a1"] + self.RangeH["a1"]/2+1)
            plt.xlabel("a1",fontsize=20)
            plt.ylim(self.x_dict["b1"] - self.RangeH["b1"]/2-0.05,self.x_dict["b1"] + self.RangeH["b1"]/2+0.05)
            plt.ylabel("b1",fontsize=20)
            plt.title("Weight Map",fontsize=24)
            cm = self.generate_cmap(['mediumblue', 'limegreen', 'orangered'])
            plt.scatter(self.x.T[0], self.x.T[1], s=15, c=w_, cmap=cm)
            
            # カラーバーを表示
            ax=plt.colorbar()
            ax.set_label("Probability",fontsize=20)
            plt.show() 

        """
        elif (self.ParamH["Bz"] == 1) and (self.ParamH["Ac1"] == 1) :
            #重みの二次元表示
            #Bz vs Ac1 ver.
            w_=self.w.reshape(self.w.shape[0],)
            plt.xlim(self.x_dict["Bz"] - self.RangeH["Bz"]/2-1,self.x_dict["Bz"] + self.RangeH["Bz"]/2+1)
            plt.xlabel("Bz",fontsize=20)
            plt.ylim(self.x_dict["Ac1"] - self.RangeH["Ac1"]/2-0.05,self.x_dict["Ac1"] + self.RangeH["Ac1"]/2+0.05)
            plt.ylabel("Ac1",fontsize=20)
            plt.title("Weight Map",fontsize=24)
            cm = self.generate_cmap(['mediumblue', 'limegreen', 'orangered'])
            plt.scatter(self.x.T[8], self.x.T[9], s=15, c=w_, cmap=cm)
            
            # カラーバーを表示
            ax=plt.colorbar()
            ax.set_label("Probability",fontsize=20)
            plt.show()
        """



    
    def main(self, exp_random):
        
        
        """
        #debug
        #格納リスト
        """
        
        self.select_freq = []
        
        #Project Fileから読み取る
        self.read_project()
        
        #各パラメータを初期化
        self.initialize()
        
        #パーティクル、実験設計候補を作成
        self.preparation()
        
        #ベイズ推定
        for i in range(self.ex):
            
            print("experiment#", i)
            self.i=i
            self.trial_number = i
            (self.i_list).append(i)
            
            self.flag1 = False
            
            #時間計測
            tim0=time.time()
                
            #実験パラメータを範囲内に収める
            if self.exp_select=="all":
                for j in range(2):
                    self.Exp_limit(j)
            else:
                self.Exp_limit(0)
            
            #パーティクルのリサンプリング
            print("Neff_theshold : {}".format(len(self.w)*self.resample_threshold))
            print("Neff : {}".format(1/np.sum(self.w*self.w)))
            
            if (1/np.sum(self.w*self.w)<len(self.w)*self.resample_threshold) : #パーティクルの再配分
                self.w,self.x=self.resample_particle()
                
                #リサンプリングしたかどうか
                self.resampling_list.append(1)
                self.flag1 = True
                
            if self.sweep == 1:
                self.flag1 = True
            
            #リサンプリングしなかった場合,0を代入する
            #結果描画用の配列作成
            if len(self.resampling_list) == self.i:
                self.resampling_list.append(0)
            
            #実験を選ぶ
            if self.i > exp_random-1:
                
                #ptableの作成
                if self.flag1==True or self.i==0 :
                    #self.Prob_Lookup_parallel() #ptableを用意する  
                    #self.Prob_Lookup()
                    
                    #TFでptable作成
                    print("expsim_start")
                    self.Expsim_gpu()
                print("======================ptable_end=====================")
                
                #効用の計算
                if self.exp_select=="all":
                    
                    #複数の実験行う場合
                    self.UtilIG_bayes_risk_all()
                    
                else:
                    
                    #一つの実験しか行わない場合
                    #self.UtilIG_bayes_risk_one()

                    #self.UtilIG_cupy()
                    self.UtilIG_normal()
            
            #ランダムに実験を選ぶ
            else:
                self.Exp_random()
            
            
            print("===============選んだ実験===============")
            print(self.C_best)
            self.select_freq.append(self.C_best[4])
            print("end simulation\n")
            
            #0:simulation, 1:exp
            if self.mode == 1:
                
                #num_repeat=0
                
                #C_bestをUnicornに伝える
                self.storage_data()

                self.storage_exp()
                    
                #実験結果がでるまで待機
                self.Read_data()
                
            
            if self.mode == 1:
                #ベイズ推定
                #self.Update()
                #self.Update_photon()
                self.Update_cupy()
                
            if self.mode == 0:
                #self.Update_cupy()
                self.Update_normal()
                #self.Update()
            
            #ベイズリスクの計算
            self.Bayes_risk()
            print("現在のベイズリスク",np.exp(self.risk[self.i]))
        
            #推定したハミルトニアンを出力
            xout=self.Mean(self.w,self.x)
            print("推定したハミルトニアン",xout) #推定したハミルトニアンを出力
            
            #結果の表示
            self.Show_result()
            
            #推定パラメータの推移
            self.Estimation_result()
            
            #1推定にかかった時間
            tim1=time.time()
            print (tim1-tim0,"sec","\n")
            tim0=tim1
            
            #推定を続けるか判断する
            if np.exp(self.risk[self.i]) < self.bayes_threshold:
                #m.END_file()
                print("=======================End of estimation======================")
                break
            
            if np.max(self.U) == 0:
                break
            
            
            print("最も確からしいハミルトニアン",xout) #最終的に最も確からしいと考えられたハミルトニアンを出力
            #self.show_hyper_parameter()
            
        if self.mode == 1:
            #推定終了
            self.END_file()
            