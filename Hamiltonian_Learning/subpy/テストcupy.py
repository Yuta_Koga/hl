# -*- coding: utf-8 -*-
"""
Created on Sun Feb  3 18:48:09 2019

@author: a319
"""

import cupy as cp
cp.cuda.set_allocator(cp.cuda.MemoryPool().malloc)
import numpy as np
from scipy.stats import norm as norm # 正規分布
import matplotlib.pyplot as plt
import time


"""
numpyでの指定域内確率計算([x2:x1]の範囲にある確率)
本当は積分だけど、cupyの実装に合わせて近似式を利用
P=(|f(x2)+f(x1)|/2)*(x2-x1)

"""
"""
def norm(x, step, mean, div):#step=x2-x1,mean=平均値,div=分散
    y1=np.exp((-1*(x-mean)**2)/(2*div**2))/(np.sqrt(2*np.pi*div**2))
    y2=np.exp((-1*((x+step)-mean)**2)/(2*div**2))/(np.sqrt(2*np.pi*div**2))
    y=np.abs(y2+y1)*step/2
    return(y)
"""


norm_kernel = cp.ElementwiseKernel(
            in_params='float32 x,float32 step, float32 mean,float32 div',
            out_params='float32 y',
            operation=\
            '''
            float y1,y2;
            y1=expf((-1.0*(x-mean)*(x-mean))/(2.0*div*div))/(sqrt(2.0*3.14159265*div*div));
            y2=expf((-1.0*((x+step)-mean)*((x+step)-mean))/(2.0*div*div))/(sqrt(2.0*3.14159265*div*div));
            y=abs(y2+y1)*step/2;
            ''',
            name='norm_kernel')



#weight particle
w_n = 10000
w_array = np.ones((1,w_n),dtype=float) / w_n

#experiment particle(C)
C_n = 1000


print(cp.cuda.alloc(10))

#ptable
p = ( 1.0-0.7) * np.random.rand(w_n,C_n) + 0.7
p = p.reshape([1, w_n, C_n])
p_cp = cp.asarray(p, dtype=cp.float32)#後々P_cubeを転置することになる際に、順番問題が面倒だからここでpを転置してまふ

#分散
div = 0.04 #cp.array([0.04],cp.float32)

#x
p_step = 0.1
d = np.arange(-0.2, 1.2, p_step)
d_cp = cp.array([[d]], cp.float32).reshape([-1, 1, 1]) #なんかここも転置してたほうがよさげ

t1 = time.time()
temp_cp = norm_kernel(d_cp, p_step, p_cp, div) #各ｐから導出される区間確率をGPUをつかって導出
t2 = time.time()
print("cupy計算時間:{}s".format(t2-t1))
p_cube_cp = cp.asnumpy(temp_cp)


"""
#debug numpyと結果が一致するか
"""
"""
for i in range(7):
    for j in range(w_n):
        for k in range(C_n):
            p_np[i, j, k] = norm(d[i], p_step, p[:,j,k], div)
"""

#平均値
p_np = p
d_np = d.reshape([-1, 1, 1])
d_np_ = d_np - p_step

t1 = time.time()
p1 = norm.cdf(d_np, p_np, div)
p2 = norm.cdf(d_np_, p_np, div)
t2 = time.time()
p_cube_np = p2 - p1

print("numpy計算時間:{}s".format(t2-t1))









"""
print(p[12,72])
plt.plot(x,p_cube[12,72])
plt.show()
print(np.sum(p_cube[12,72]))
"""





"""
temp2=temp.reshape(100,100,len(x))
plt.plot(x,temp2[0,0])
plt.show()
"""

"""

gate_photon=10000
ref_photon=10000
shotnoise_std=3.5/np.sqrt(gate_photon+ref_photon)

pro1=np.arange(0.5,1.2,0.01)
pro2=np.arange(0.51,1.21,0.01)

wav=norm.pdf(pro1,loc=0.75,scale=0.04)
plt.plot(pro,wav)
plt.show()

temp1=norm.cdf(pro1, loc=0.75, scale=0.04)
temp2=norm.cdf(pro2, loc=0.75, scale=0.04)
print(otesei_norm(pro1[21],0.75,0.04))
plt.plot(otesei_norm(pro1,0.75,0.04))
plt.show()
"""