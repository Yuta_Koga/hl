# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 19:17:22 2019

@author: a319
"""

import pycuda.autoinit
import pycuda.driver as drv
import numpy as np

from pycuda.compiler import SourceModule


mod = SourceModule("""
__global__ void norm(float *x, float step, float *mean, float div, float *y)
{
  float y1,y2;
  int i = threadIdx.x
  int j = threadIdx.y
  int k = threadIdx.z
  y1[i][j][k] = expf((-1.0*(x[i][j][k]-mean[i][j][k])*(x[i][j][k]-mean[i][j][k]))/(2.0*div*div))/(sqrt(2.0*3.14159265*div*div));
  y2[i][j][k] = expf((-1.0*((x[i][j][k]+step)-mean[i][j][k])*((x[i][j][k]+step)-mean[i][j][k]))/(2.0*div*div))/(sqrt(2.0*3.14159265*div*div));
  y[i][j][k] = abs(y2[i][j][k]+y1[i][j][k])*step/2;
}
""")

multiply_them = mod.get_function("norm")

#weight particle
w_n = 100
w_array = np.ones((1,w_n),dtype=float) / w_n

#experiment particle(C)
C_n = 100

#ptable
p = ( 1.0-0.7) * np.random.rand(w_n,C_n) + 0.7


#分散
div = 0.04 #cp.array([0.04],cp.float32)
div = div.astype(np.float32)

#x
p_step = 0.1
p_step = p_step.astype(np.float32)
d = np.arange(-0.2, 1.2, p_step)
d = np.einsum("i, jk -> ijk", d, np.ones([w_n,C_n]))

#平均値
p = np.einsum("i, jk -> ijk", np.ones(d.shape[0]), p)
p_np = p
p_np = p_np.astype(np.float32)

d_np = d
d_np = d_np.astype(np.float32)

y = np.empty([d_np.shape[0], w_n, C_n], dtype = np.float32)
multiply_them(
        drv.In(d_np), drv.In(p_step), drv.In(p_np) ,drv.In(div), drv.Out(y), 
        block=([d_np.shape[0], w_n, C_n]), grid=(1,1))

