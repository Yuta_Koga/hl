# -*- coding: utf-8 -*-
"""
Created on Mon Jan 28 14:39:34 2019

@author: a319
"""
#ベイズ推定に必要な関数がまとまったクラス
from Bayes_function_TF_01 import *

#GPU計算に必要なライブラリ
import pycuda.autoinit
import pycuda.gpuarray as gpuarray
import skcuda.cusolver as cusolver
import skcuda.linalg as culinalg
import skcuda.cublas as cublas

#行列計算に必要なライブラリ
import scipy.linalg as sp
import time
from numpy.random import *
import numpy as np

culinalg.init()

class Bayes_GPU(Bayes_Function):
    
    def __init__(self):
        Bayes_Function.__init__(self)
        
    def Create_Hamiltonian(self, free, init):
        x = self.x
        n = len(x)
        
        if self.sweep == 0:
            m = self.n_exp(self.exp_select)
            
        else:
            m = 1
        
        t0 = time.time()
        
        #実験設計分パーティクルを拡張
        x_m = np.einsum("i, jk -> jik", np.ones(m), x)
        x_m = x_m.reshape(n*m,x.shape[1])
        x2 = x_m.T
        
        x2 = x2.astype(np.complex64)
        
        """
        #システムハミルトニアンを作成
        """
        
        #炭素数に応じてハミルトニアンの骨格を拡張
        print(len(self.Ac_list))
        if len(self.Ac_list) != 0:
            
            #炭素数分恒等行列を作成
            C_list = []
            for i in range(len(self.Ac_list)):
                C_list.append(2)
                self.C_mat=Qobj(qeye(2**len(self.Ac_list)),dims=[C_list,C_list])
            
            #炭素のハイパーファインのシード作成
            Hint_ec_seed = []
            for i in range(len(self.Ac_list)):
                C_z=tensor(Sz,III) #電子、窒素
                for j in range(i):
                    C_z=tensor(C_z,II)
                C_z=tensor(C_z,sigz)
                for k in range(len(self.Ac_list)-i-1):
                    C_z=tensor(C_z,II)
                Hint_ec_seed.append(C_z.full())
                
            #各シードに炭素分の恒等演算子を追加
            He_seed =       (tensor(Sz*Sz,III,self.C_mat)).full()
            Hn_seed =       (tensor(III,Sz*Sz,self.C_mat)).full()
            Hint_en_seed =  (tensor(Sz,Sz,self.C_mat)).full()
            HB_seed =       (tensor(Sz,III,self.C_mat)).full()
            
            H_seed = [He_seed, Hint_en_seed, Hn_seed, HB_seed]
            for i in range(len(self.Ac_list)):
                H_seed.append(Hint_ec_seed[i])
                
            H_seed = np.array(H_seed)
        
        else:
            He_seed =       (tensor(Sz*Sz,III)).full()
            Hn_seed =       (tensor(III,Sz*Sz)).full()
            Hint_en_seed =  (tensor(Sz,Sz)).full()
            HB_seed =       (tensor(Sz,III)).full()
        
            H_seed = [He_seed, Hint_en_seed, Hn_seed, HB_seed]
            H_seed = np.array(H_seed)
        
        H_seed = H_seed.astype(np.complex64)
        
        #ハミルトニアンの大きさ
        seed_size=He_seed.shape[0]
        
        x3 = x2[5:,:].T
        H_tensor = np.einsum("ij, jkl -> ijkl", x3, H_seed)
        H_tensor = (np.sum(H_tensor, axis = 1)).astype(np.complex64)
        H = np.reshape(H_tensor,(seed_size*n*m,seed_size))
        

        """
        #ドライブハミルトニアンを作成
        """
        C = self.C[0]

        x1 = x[:,:5]

        #マイクロ波を印加した場合のドライブハミルトニアン
        if free == False:
            
            if self.sweep == 0:
            
                #ラビ周波数
                if self.wire == 1:
                    Omega = np.array([self.R_V_func(x1[i,0],x1[i,1],C[j,0])/2.0 for i in range(n) for j in range(m)])
                    
                elif self.wire == 2:
                    Omega = np.array([self.R_V_func(x1[i,2],x1[i,3],C[j,1])/2.0 for i in range(n) for j in range(m)])
                    
                #MW周波数
                MWf = np.array([C[j,4] for i in range(n) for j in range(m)])
                
                """
                #時間発展の時間を用意
                """
                self.t_list = np.empty([n * m, 1],dtype = np.complex64)
                for i in range(n):
                    for j in range(m):
                        self.t_list[i*m+j,:] = self.C[0][j][3]
                
            elif self.sweep == 1:
                
                self.sweep_index = int(np.mod(self.i, self.n_exp(self.exp_select)))
                print(self.sweep_index)
                
                #ラビ周波数
                if self.wire == 1:
                    Omega = np.array([self.R_V_func(x1[i,0],x1[i,1],C[self.sweep_index,0])/2.0 for i in range(n)])
                    
                elif self.wire == 2:
                    Omega = np.array([self.R_V_func(x1[i,2],x1[i,3],C[self.sweep_index,1])/2.0 for i in range(n)])
                    
                #MW周波数
                MWf = np.array([C[self.sweep_index,4] for i in range(n)])
                
                """
                #時間発展の時間を用意
                """
                self.t_list = np.empty([n, 1],dtype = np.complex64)
                for i in range(n):
                    self.t_list[i,:] = self.C[0][self.sweep_index][3]
            
            """
            #debug ラビ周波数
            """
            self.Omega = Omega
            
            if len(self.Ac_list) != 0:
                Hd_seed = (tensor(Sx,III,self.C_mat)).full()
            else:
                Hd_seed = (tensor(Sx,III)).full()
                
            Hd_tensor = np.einsum("i, jk -> ijk", Omega, Hd_seed)
            
            Hd_tensor = Hd_tensor.astype(np.complex64)
            
            self.Hd_tensor = Hd_tensor
            
            Hd = np.reshape(Hd_tensor,(seed_size*n*m,seed_size))
            
            """
            #回転座標系に乗るためのハミルトニアンを作成
            """
            
            if len(self.Ac_list) != 0:
                Hf_seed = (tensor(Sz*Sz,III,self.C_mat)).full()
            else:
                Hf_seed = (tensor(Sz*Sz,III)).full()
                
            Hf_tensor = np.einsum("i, jk -> ijk", MWf, Hf_seed)
            
            self.Hf_tensor = Hf_tensor
            
            Hf_tensor = Hf_tensor.astype(np.complex64)
            
            Hf = np.reshape(Hf_tensor,(seed_size*n*m,seed_size))
            
            self.HI_list = H + Hd - Hf
            
        #free evolution
        elif free == True:
                    
            if self.sweep == 0:
                #MW周波数
                MWf = np.array([C[j,4] for i in range(n) for j in range(m)])
                
                """
                #時間発展の時間を用意
                """
                
                self.t_list = np.empty([n * m, 1],dtype = np.complex64)
                for i in range(n):
                    for j in range(m):
                        self.t_list[i*m+j,:] = self.C[0][j][5]
                
            elif self.sweep == 1:
                
                self.sweep_index = int(np.mod(self.i, self.n_exp(self.exp_select)))
                
                #MW周波数
                MWf = np.array([C[self.sweep_index,4] for i in range(n)])
                
                """
                #時間発展の時間を用意
                """
                self.t_list = np.empty([n, 1],dtype = np.complex64)
                
                for i in range(n):
                    self.t_list[i,:] = self.C[0][self.sweep_index][5]
            
            """
            #回転座標系に乗るためのハミルトニアンを作成
            """
            
            if len(self.Ac_list) != 0:
                Hf_seed = (tensor(Sz*Sz,III,self.C_mat)).full()
            else:
                Hf_seed = (tensor(Sz*Sz,III)).full()
                
            Hf_tensor = np.einsum("i, jk -> ijk", MWf, Hf_seed)
            
            self.Hf_tensor = Hf_tensor
            
            Hf_tensor = Hf_tensor.astype(np.complex64)
            
            Hf = np.reshape(Hf_tensor,(seed_size*n*m,seed_size))
            
            self.HI_list = H - Hf
        
        """
        #初期状態を複数用意
        """
        if init == True:
            #初期化
            self.rho_init()
            
            rho0_seed = (self.rho).full()
            
            rho0_tensor = np.einsum("i, jk -> ijk", np.ones(n*m), rho0_seed)
            
            rho0_tensor = rho0_tensor.astype(np.complex64)
            
            self.rho0_list = rho0_tensor
        
        """
        #射影演算子を複数実装
        """
        
        self.S0_list = self.rho0_list * seed_size/3
        
        self.S0_list = self.S0_list.astype(np.complex64)
        
        t1 = time.time()
        print("Creating Hamiltonian time:{}s".format(t1 - t0))
        
        
    def expH(self):
    
        """
        #GPUで時間発展を計算
        """
        x = self.x
        n = len(x)
        
        if self.sweep == 0:
            m = self.n_exp(self.exp_select)
            
        else:
            m = 1
        
        print("Calculation by GPU")
        t0 = time.time()
        
        #変数の設定
        All_batchSize = int(self.HI_list.shape[0]/self.HI_list.shape[1])
        l = self.HI_list.shape[1]
        
        repeat = int(All_batchSize/150000) + 1
        sub_repeat = np.mod(All_batchSize, 150000)
        print(repeat, sub_repeat)
        
        
        
        #シミュレーション
        for j in range(repeat):
            
            handle = cusolver.cusolverDnCreate()
            
            if j < repeat - 1:
                batchSize = 150000
            
            else:
                batchSize = sub_repeat
            
            self.batchSize = batchSize
            
            #計測スタート
            #t1 = time.time()
            
            #CPUからGPUに移す
            if repeat > 1:
                if j < repeat - 1:
                    x_gpu = gpuarray.to_gpu(self.HI_list[l*j*batchSize:l*(j+1)*batchSize, :])
                    
                else:
                    #x_gpu = gpuarray.to_gpu(self.HI_list[repeat*batchSize:, :])
                    x_gpu = gpuarray.to_gpu(self.HI_list[l*(repeat - 1)*150000:, :])
                    
            elif repeat == 1:
                x_gpu = gpuarray.to_gpu(self.HI_list)
            
            # Set up output buffers:
            w_gpu = gpuarray.empty((batchSize, l), dtype = np.float32)
            
            # Set up parameters
            #オブジェクトのリファレンスのようなもの？
            param = cusolver.cusolverDnCreateSyevjInfo()
            cusolver.cusolverDnXsyevjSetTolerance(param, 1e-7)
            cusolver.cusolverDnXsyevjSetMaxSweeps(param, 15)
            
            # Set up work buffers:
            lwork = cusolver.cusolverDnCheevjBatched_bufferSize(handle, 'CUSOLVER_EIG_MODE_VECTOR',
                                                'u', l, x_gpu.gpudata, l,
                                                w_gpu.gpudata, param, batchSize)
            
            workspace_gpu = gpuarray.empty(lwork, dtype = np.complex64) #zeros
            
            info = gpuarray.empty(batchSize, dtype = np.int32) #zeros
            
            # Compute:
            cusolver.cusolverDnCheevjBatched(handle, 'CUSOLVER_EIG_MODE_VECTOR',
                                   'u', l, x_gpu.gpudata, l,
                                    w_gpu.gpudata, workspace_gpu.gpudata,
                                    lwork, info.gpudata, param, batchSize)
            
            self.lwork = lwork
            self.info = info
            self.param = param
            self.batchSize = batchSize
            
            # Print info
            tmp = info.get()
            if any(tmp):
                print("the following job did not converge: %r", np.nonzero(tmp)[0])
            else:
                print("all jobs converged")
            
            
            # Destroy handle
            cusolver.cusolverDnDestroySyevjInfo(param)
            cusolver.cusolverDnDestroy(handle)
            
            #GPUからCPUに情報を伝達
            Q = x_gpu.get()
            W = w_gpu.get()
            #print(W)
            #debug
            Q_cpu = np.empty((batchSize, l, l), dtype = np.complex64)
            Q_inv_cpu = np.empty((batchSize, l, l), dtype = np.complex64)
            W_cpu = np.empty((batchSize, l, l), dtype = np.complex64)
            
            for i in range(batchSize):
                Q_cpu[i,:,:] = Q[i*l:(i+1)*l,:].T.copy()
                Q_inv_cpu[i,:,:] = np.conjugate(Q[i*l:(i+1)*l,:].copy())
                w = W[i,:].copy()
                w = -1j * self.t_list[i] * 2 * np.pi * w
                w = np.exp(w)
                w = np.diag(w)
                W_cpu[i,:,:] = w
            
            self.Q_cpu = Q_cpu
            self.W_cpu = W_cpu
            
            #===============================積を計算========================================
            
            def bptrs(a):
                """
                Pointer array when input represents a batch of matrices.
                """
            
                return gpuarray.arange(a.ptr,a.ptr+a.shape[0]*a.strides[0],a.strides[0],
                            dtype=cublas.ctypes.c_void_p)
            
            handle = cublas.cublasCreate()
            
            C = np.empty((batchSize, l, l), dtype = np.complex64)
            U = np.empty((batchSize, l, l), dtype = np.complex64)
            E = np.empty((batchSize, l, l), dtype = np.complex64)
            F = np.empty((batchSize, l, l), dtype = np.complex64)
            G = np.empty((batchSize, l, l), dtype = np.complex64)
            H = np.empty((batchSize, l, l), dtype = np.complex64)
            
            
            #CPUからGPUに移す
            Q_gpu = gpuarray.to_gpu(Q_cpu)
            Q_inv_gpu = gpuarray.to_gpu(Q_inv_cpu)
            W_gpu = gpuarray.to_gpu(W_cpu)
            C_gpu = gpuarray.to_gpu(C) #バッファ
            U_gpu = gpuarray.to_gpu(U) #バッファ
            E_gpu = gpuarray.to_gpu(E) #バッファ
            F_gpu = gpuarray.to_gpu(F) #バッファ
            G_gpu = gpuarray.to_gpu(G) #時間発展後の状態
            
            if repeat > 1:
                if j < repeat - 1:
                    S0_list_temp = self.S0_list[j*batchSize:(j+1)*batchSize, :, :]
                    rho0_list_temp = self.rho0_list[j*batchSize:(j+1)*batchSize, :, :]
                    
                else:
                    S0_list_temp = self.S0_list[(repeat - 1)*150000:, :, :]
                    rho0_list_temp = self.rho0_list[(repeat - 1)*150000:, :, :]
                    
            else:
                S0_list_temp = self.S0_list
                rho0_list_temp = self.rho0_list
            
            S0_gpu = gpuarray.to_gpu(S0_list_temp) #ms=0の射影演算子
            R0_gpu = gpuarray.to_gpu(rho0_list_temp) #初期状態
            
            #一回目の積
            #t1 = time.time()
            #計算に必要なパラメータ
            transa = "T" #T
            transb = "T" #T
            m = l
            n = l
            k = l
            alpha = np.complex64(1.0)
            Q_arr = bptrs(Q_gpu)
            lda = l
            W_arr = bptrs(W_gpu)
            ldb = l
            C_arr = bptrs(C_gpu)
            ldc = l
            beta = np.complex64(0.0)
            batchCount = batchSize
            
            Q_inv_arr = bptrs(Q_inv_gpu)
            
            
            
            cublas.cublasCgemmBatched(handle, transa, transb, m, n, k,
                                   alpha, W_arr.gpudata, lda, Q_inv_arr.gpudata, ldb, beta, C_arr.gpudata, ldc, batchCount)
            
            C_cpu = C_gpu.get()
            
            for i in range(batchSize):
                C_cpu[i,:,:] = C_cpu[i,:,:].T
            
            #時間発展演算子計算
            C_gpu = gpuarray.to_gpu(C_cpu)
            C_arr = bptrs(C_gpu)
            U_arr = bptrs(U_gpu)
            
            transc = "T" #T
            transb = "T" #N
            
            cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                                   alpha, Q_arr.gpudata, lda, C_arr.gpudata, ldb, beta, U_arr.gpudata, ldc, batchCount)
            
            U_cpu = U_gpu.get()
            
            
            for i in range(batchSize):
                U_cpu[i,:,:] = U_cpu[i,:,:].T
            
            #転置
            #U_cpu = np.einsum("ijk -> ikj", U_cpu)
            
            self.U_cpu = U_cpu
            
            #逆時間発展演算子
            U_inv_cpu = np.conjugate(U_cpu)
            
            for i in range(batchSize):
                U_inv_cpu[i,:,:] = U_inv_cpu[i,:,:].T
            
            #転置
            #U_inv_cpu = np.einsum("ijk -> ikj", U_inv_cpu)
                
            self.U_inv_cpu = U_inv_cpu
            
            #左から時間発展演算子をかける
            E_arr = bptrs(E_gpu)
            R0_arr = bptrs(R0_gpu)
            
            transc = "T" #T
            transb = "T" #T
            
            cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                                   alpha, U_arr.gpudata, lda, R0_arr.gpudata, ldb, beta, E_arr.gpudata, ldc, batchCount)
            
            
            E_cpu = E_gpu.get()
            
            for i in range(batchSize):
                E_cpu[i,:,:] = E_cpu[i,:,:].T
            
            #転置
            #E_cpu = np.einsum("ijk -> ikj", E_cpu)
            
            #右から時間発展演算子の逆行列をかける
            transc = "T" #T
            transb = "T" #T
            
            E_gpu = gpuarray.to_gpu(E_cpu)
            U_inv_gpu = gpuarray.to_gpu(U_inv_cpu)
            E_arr = bptrs(E_gpu)
            F_arr = bptrs(F_gpu)
            U_inv_gpu_arr = bptrs(U_inv_gpu)
            
            
            cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                                   alpha, E_arr.gpudata, lda, U_inv_gpu_arr.gpudata, ldb, beta, F_arr.gpudata, ldc, batchCount)
            
            
            F_cpu = F_gpu.get()
            
            for i in range(batchSize):
                F_cpu[i,:,:] = F_cpu[i,:,:].T
            
            #転置
            #F_cpu = np.einsum("ijk -> ikj", F_cpu)
            
            self.F_cpu = F_cpu
            
            #ms=0で射影する
            transc = "T" #T
            transb = "T" #T
            
            F_gpu = gpuarray.to_gpu(F_cpu)
            #S0_gpu = gpuarray.to_gpu(self.S0_list)
            S0_arr = bptrs(S0_gpu)
            F_arr = bptrs(F_gpu)
            G_arr = bptrs(G_gpu)
            
            cublas.cublasCgemmBatched(handle, transc, transb, m, n, k,
                                   alpha, S0_arr.gpudata, lda, F_arr.gpudata, ldb, beta, G_arr.gpudata, ldc, batchCount)
            
            G_cpu = G_gpu.get()
            
            for i in range(batchSize):
                G_cpu[i,:,:] = G_cpu[i,:,:].T
            
            #転置
            #G_cpu = np.einsum("ijk -> ikj", G_cpu)
            
            self.G_cpu = G_cpu
            
            #handleの消去
            cublas.cublasDestroy(handle)
            
            if j < repeat - 1:
                self.rho0_list[j*batchSize:(j+1)*batchSize, :, :] = self.G_cpu
            else:
                self.rho0_list[j*150000:, :, :] = self.G_cpu
            
        t1 = time.time()
        print("Calculating Time evolution time:{}s".format(t1 - t0))
        print("End calculation by gpu")
        
        
    def trace(self):
        """
        #時間発展した状態をms=0で射影測定
        """
        
        x = self.x
        n = len(x)
        
        if self.sweep == 0:
            m = self.n_exp(self.exp_select)
        
        else:
            m = 1
        
        #ptable格納配列を用意
        self.ptable = np.empty([n*m,1])
        
        #traceをとる
        for i in range(int(n*m)):
            self.ptable[i] = np.real(np.trace(self.rho0_list[i, :, :]))
        
        if self.sweep == 0:
            self.ptable = [self.ptable.reshape([self.n_particles(), self.n_exp(self.exp_select)])]
            
        elif self.sweep == 1:
            self.ptable = [self.ptable.reshape([self.n_particles(), 1])]
        
    def Expsim_gpu(self):
        
        if self.exp_flag == "rabi":
            self.Create_Hamiltonian(free=False, init=True)
            self.expH()
            self.trace()
            
        elif self.exp_flag == "ramsey":
            #上げる
            self.Create_Hamiltonian(free=False, init=True)
            self.expH()
            
            #free evolution
            self.Create_Hamiltonian(free=True, init=False)
            self.expH()
            
            #下げる
            self.Create_Hamiltonian(free=True, init=False)
            self.expH()
            self.trace()
            
