# -*- coding: utf-8 -*-
"""
Created on Wed Jan  9 11:28:04 2019

@author: a319
"""

#ライブラリのimport
import datetime
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import numpy as np
from scipy.stats import multivariate_normal
mpl.rcParams['figure.autolayout'] = True
mpl.rcParams['axes.grid'] = True

#日付を取得
date = datetime.datetime.today().strftime("%Y/%m/%d/%H/%M/%S")#フォーマットの指定
date = date.split("/")
date = date[0] + date[1] + date[2]


"""
#フォルダの作成
if os.path.exists(cd + "\\" + date) == False:
    od = os.getcwd()
    cd = "C:\\data&prog\\koga\\data"
    os.chdir(cd)
    os.mkdir(cd + "\\" + date)
    os.chdir(od)

os.chdir(cd + "\\" + date)
l = np.array([[1,2,3],[2,4,65]])
np.savetxt("Test"+"01"+".txt",l,newline="\n",delimiter=",")
os.chdir(od)

os.chdir(cd + "\\" + date)
l = np.array([[1,2,3],[2,4,65]])
plt.plot(l[0], l[1])
plt.savefig("test")
os.chdir(od)
"""

"""
fig = plt.figure()
ax = Axes3D(fig)
#ax = fig.add_subplot(111, projection = '3d')
x = np.array([j for i in range(10) for j in range(10)]).reshape([-1,1])
y = np.array([i for i in range(10) for j in range(10)]).reshape([-1,1])
z = np.array([i for i in range(10) for j in range(10)]).reshape([-1,1])
#ax.scatter(x, y, z, c=z)
#A=ax.scatter3D(x, y, z, c=z)
surf = ax.plot_surface(x, y, z, cmap='bwr', linewidth=0)
ax.set_xlim(0,10)
ax.set_ylim(0,10)
ax.set_zlim(0,10)
plt.xlabel("x", fontsize=20)
plt.ylabel("y", fontsize=20)
#plt.zlabel("z", fontsize=20)
fig.colorbar(surf)
plt.show()
"""

import copy as cp
from itertools import chain
import numpy as np

par = []

#各試行でのパーティクル追加
fig = plt.figure()
ax = Axes3D(fig)
ax = fig.add_subplot(11, projection='2d')  
for i in range(103):
    cd = "C:\\data&prog\\koga\\data\\201902060422_Project_a1_b1_19"
    f=np.genfromtxt(cd + "\\" + "Particle" + str(i) +  ".txt",delimiter=",",encoding="utf-8")


    
    #磁場のみ取り出す
    d = []
    for j in range(len(f)):
        d.append(float(f[j][8]))
    
    d = np.array(d)

    par=d

    
    #各試行での重み追加
    wei = []

    cd = "C:\\data&prog\\koga\\data\\201902060422_Project_a1_b1_19"
    g=np.genfromtxt(cd + "\\" + "Weight" + str(i) +  ".txt",encoding="utf-8")

    w = []
    for j in range(len(g)):
        w.append(float(g[j]))
        
    w = np.array(w)
    wei=w
    

    y=np.array([i]*par.shape[0])
    
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")
  
ax.scatter(par,y,wei)
    
"""
#分散、標準偏差の描画
var = []
std = []
for i in range(103):
    w = wei[i]
    p = par[i]
    mu = np.sum(w*p)
    p_ = (p - mu)**2
    var.append(np.sum(w*p_)/w.shape[0])
    std.append(np.sqrt(np.sum(w*p_)/w.shape[0]))
"""   
"""
fig = plt.figure(figsize=(10,10))
t = np.arange(0,103,1)
plt.yscale('log')
plt.xlabel("trial number", fontsize=26)
plt.ylabel("Bayes risk",fontsize=26)
plt.title("Bayes risk",fontsize=30)
plt.plot(t,var)
plt.show()
    
fig = plt.figure(figsize=(10,10))
t = np.arange(0,103,1)
plt.yscale('log')
plt.xlabel("trial number", fontsize=26)
plt.ylabel("std[Bz]/(MHz)",fontsize=26)
plt.title("Uncertainty of Bz",fontsize=30)
plt.plot(t,std)
plt.show()
""" 

"""

x = np.array([j for i in range(10) for j in range(10)]).reshape([10,10])
y = np.array([i for i in range(10) for j in range(10)]).reshape([10,10])
z = np.array([i for i in range(10) for j in range(10)]).reshape([10,10])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
surf = ax.plot_surface(x, y, z, cmap='bwr', linewidth=0)
fig.colorbar(surf)
ax.set_title("Surface Plot")
ax.set_xlim(0,10)
fig.show()
"""















